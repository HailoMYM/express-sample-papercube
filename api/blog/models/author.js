const mongoose = require('mongoose');

const authorSchema = new mongoose.Schema({
  username: {
    type: String,
    required: true,
    minlength: 1,
    maxlength: 255,
    unique: true,
  },
  email: {
    type: String,
    required: true,
    minlength: 5,
    maxlength: 255,
  },
  name: { type: String, required: true, minlength: 1, maxlength: 255 },
  lastname: { type: String, required: true, minlength: 1, maxlength: 255 },
  posts: {
    type: [mongoose.Schema.Types.ObjectId]
  },
  active: { type: Boolean, default: true },
});

const Author = mongoose.model('Author', authorSchema);

module.exports = {
  Author,
  authorSchema
};
