const mongoose = require('mongoose');
const { authorSchema } = require('./author')

authorSchema.remove('posts');

const postSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
    minlength: 1,
    maxlength: 255,
    unique: true,
  },
  content: {
    type: String,
    required: true,
    minlength: 1,
  },
  active: { type: Boolean, default: true },
  author: {
    type: authorSchema,
    required: true,
  }
}, {
  timestamps: true,
});

const Post = mongoose.model('Post', postSchema);

module.exports = {
  Post,
};
