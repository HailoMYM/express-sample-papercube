const express = require('express');
const router = express.Router();

router.use('/authors', require('./author'));
router.use('/posts', require('./post'));

module.exports = router;
