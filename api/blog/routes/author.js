const express = require('express');
const router = express.Router();

const { Author } = require('../models/author');

// ? The root of the urls is /api/authors

router.get('/', async function (req, res) {
  const authors = await Author.find();
  res.send(authors);
});

router.get('/:id', function (req, res) {
  res.send(`Read author with id: ${req.params.id}`);
});

router.post('/', async function (req, res) {
  const author = new Author(req.body);
  await author.save()
  res.send(author);
});

router.put('/:id', function (req, res) {
  res.send(`Update author with id: ${req.params.id} using data ${JSON.stringify(req.body)}`)
});

router.delete('/:id', function (req, res) {
  res.send(`Delete author with id: ${req.params.id}`)
});

module.exports = router;
