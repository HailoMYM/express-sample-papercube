var express = require('express');
var router = express.Router();

const { Post } = require('../models/post');

// ? The root of the urls is /api/posts

router.get('/', async function (req, res) {
  const posts = await Post.find();
  res.send(posts);
});

router.get('/:id', function (req, res) {
  res.send(`Read post with id: ${req.params.id}`);
});

router.post('/', async function (req, res) {
  const post = new Post(req.body);
  await post.save()
  res.send(post);
});

router.put('/:id', function (req, res) {
  res.send(`Update post with id: ${req.params.id} using data ${JSON.stringify(req.body)}`)
});

router.delete('/:id', function (req, res) {
  res.send(`Delete post with id: ${req.params.id}`)
});

module.exports = router;
